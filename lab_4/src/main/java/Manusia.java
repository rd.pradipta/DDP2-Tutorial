import java.lang.*;
import java.util.Scanner;

public class Manusia{

    private int uang = 50000;
    private float kebahagiaan = (float) 50.0; 
    private String nama = "";
    private int umur = 0;

    public Manusia(String nama, int umur)
    {
        this.nama = nama;
        this.umur = umur;
    }

    public Manusia(String nama, int umur, int uang)
    {
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
    }
// ------------------------------------------------ //

    public void setNama(String nama)
    {
        this.nama = nama;
    }
    public void setUmur(int umur)
    {
        this.umur = umur;
    }
    public void setUang(int uang)
    {
        this.uang = uang;
    }
    public void setKebahagiaan(float kebahagiaan)
    {
        this.kebahagiaan = kebahagiaan;
    }
// ------------------------------------------------ //
    public String getNama()
    {
		return this.nama;
	}
    public int getUmur()
    {
		return this.umur;
	}
    public int getUang()
    {
		return this.uang;
	}
    public float getKebahagiaan()
    {
		return this.kebahagiaan;
	}

// ------------------------------------------------ //
    public void beriUang(Manusia penerima)
    {
        int uangdikasih = 0;
        String nama_penerima = penerima.nama;

        for (int i = 0; i < nama_penerima.length(); i++)
        {
            char huruf = nama_penerima.charAt(i);
            uangdikasih += huruf;
        }
        uangdikasih *= 100;

        if (this.uang >= uangdikasih)
        {
            penerima.uang += uangdikasih;
            this.uang -= uangdikasih;

            this.kebahagiaan += (float) uangdikasih/6000;
            penerima.kebahagiaan += (float) uangdikasih/6000;

            if (this.kebahagiaan > (float) 100)
            {
                this.kebahagiaan = (float) 100;
            }
            else if (this.kebahagiaan < (float) 0)
            {
                this.kebahagiaan = (float) 0;
            }

            if (penerima.kebahagiaan > (float) 100)
            {
                penerima.kebahagiaan = (float) 100;
            }
            else if (penerima.kebahagiaan < (float) 0)
            {
                penerima.kebahagiaan = (float) 0;
            }

            String keluaran = String.format("%s memberi uang sebanyak %d kepada %s, mereka berdua senang :D",this.nama,uangdikasih,penerima.nama);
			System.out.println(keluaran);
        }
        else
        {
            String keluaran = String.format("%s ingin memberi uang kepada %s namun tidak memiliki cukup uang :(",this.nama,penerima.nama);
			System.out.println(keluaran);
        }
    }
// ---------------------------------------------------------------------------- //
    public void beriUang(Manusia penerima, int uangdikasih)
    {
        if (this.uang > uangdikasih)
        {
            this.uang -= uangdikasih;
            penerima.uang += uangdikasih;

            this.kebahagiaan += (float) uangdikasih/6000;
            penerima.kebahagiaan += (float) uangdikasih/6000;

            if (this.kebahagiaan > (float) 100)
            {
                this.kebahagiaan = (float) 100;
            }
            else if (this.kebahagiaan < (float) 0)
            {
                this.kebahagiaan = (float) 0;
            }

            if (penerima.kebahagiaan > (float) 100)
            {
                penerima.kebahagiaan = (float) 100;
            }
            else if (penerima.kebahagiaan < (float) 0)
            {
                penerima.kebahagiaan = (float) 0;
            }

            String keluaran = String.format("%s memberi uang sebanyak %d kepada %s, mereka berdua senang :D",this.nama,uangdikasih,penerima.nama);
            System.out.println(keluaran);

        }
        else
        {
            String keluaran = String.format("%s ingin memberi uang kepada %s namun tidak memiliki cukup uang :(",this.nama,penerima.nama);
			System.out.println(keluaran);
        }
    }
// ---------------------------------------------------------------------------- //
    public void bekerja(int durasi, int bebanKerja)
    {
        if(this.umur<18)
        {
            String keluaran = String.format("%s belum boleh bekerja karena masih dibawah umur D:",this.nama);
            System.out.println(keluaran);
            return ;
        }

        int pendapatan = 0;
        int bebanKerjaTotal = durasi * bebanKerja;
        if(bebanKerjaTotal <= this.kebahagiaan){
            this.kebahagiaan -= bebanKerjaTotal;
            pendapatan = bebanKerjaTotal * 10000;
            this.uang += (pendapatan);

            String out = String.format("%s bekerja full time, total pendapatan : %d",this.nama, pendapatan);
            System.out.println(out);

        }
        
        else
        {
            float durasiBaruFloat = this.kebahagiaan / bebanKerja;
            int durasiBaru = (int)durasiBaruFloat;
            bebanKerjaTotal = durasiBaru * bebanKerja;
            this.kebahagiaan -= bebanKerjaTotal;
            pendapatan = bebanKerjaTotal * 10000;
            this.uang += (pendapatan);

            String out = String.format("%s tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : %d",this.nama,pendapatan);
            System.out.println(out);
        }
    }

// ---------------------------------------------------------------------------- //
    public void rekreasi(String tempat)
    {
        int biayarekreasi = 0;
        biayarekreasi = tempat.length() * 10000;

        if (this.uang >= biayarekreasi)
        {
            this.kebahagiaan += (float) tempat.length();

            if (this.kebahagiaan > (float) 100)
            {
                this.kebahagiaan = (float) 100;
            }
            else if (this.kebahagiaan < (float) 0)
            {
                this.kebahagiaan = (float) 0;
            }

            this.uang -= biayarekreasi;
            String keluaran = String.format("%s berekreasi di %s , %s senang :)",this.nama,tempat,this.nama);
            System.out.println(keluaran);
        }
        else
        {
            String keluaran = String.format("%s tidak mempunyai cukup uang untuk berekreasi di %s :(",this.nama,tempat);
            System.out.println(keluaran);
        }
    }
// ---------------------------------------------------------------------------- //
    public void sakit (String penyakit)
    {
        this.kebahagiaan -= (float) penyakit.length();

        if (this.kebahagiaan > (float) 100)
        {
            this.kebahagiaan = (float) 100;
        }
        else if (this.kebahagiaan < (float) 0)
        {
            this.kebahagiaan = (float) 0;
        }
        String keluaran = String.format("%s terkena penyakit %s :O",this.nama,penyakit);
        System.out.println(keluaran);
    }
// ---------------------------------------------------------------------------- //

public String toString(){
    return String.format("Nama\t\t:%s\nUmur\t\t:%d\nUang\t\t:%d\nKebahagiaan\t:%.1f", this.nama, this.umur, this.uang, this.kebahagiaan);
}

}