import xoxo.XoxoView;
import xoxo.XoxoController;

/**
 * Main class that runs the Xoxo Controller.
 * @author Rd Pradipta Gitaya S
 * NPM  : 1706043361
 */
public class Lab10 {

    /**
     * The main method.
     * 
     * @param args Argument strings.
     */
    public static void main(String[] args) {
        XoxoView view = new XoxoView();
        XoxoController controller = new XoxoController(view);
        controller.run();
    }
    
}