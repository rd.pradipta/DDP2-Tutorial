package xoxo.crypto;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author Rd Pradipta Gitaya S
 * NPM  : 1706043361
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) {
        //TODO: Implement decryption algorithm
        String hasil = "";
        for (int i = 0; i < encryptedMessage.length(); i++){
            hasil += (char)(encryptedMessage.charAt(i)^((hugKeyString.charAt(i % hugKeyString.length())^seed) - 'a'));
        }
        return hasil;
        
    }
}