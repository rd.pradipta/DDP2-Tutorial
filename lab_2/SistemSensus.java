import java.util.Scanner;

public class SistemSensus{
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		
		System.out.print("SELAMAT DATANG DI PROGRAM PENCETAK DATA SENSUS\n" +
				"----------------------------------------------------------------\n" +
				"Silakan Masukkan Data yang Diperlukan\n" +
				"Untuk catatan kosong, bisa langsung tekan Enter untuk melewati\n" +
				"Rd Pradipta Gitaya S - 1706043361 - Kelas E\n" +
				"----------------------------------------------------------------\n"+
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();

		System.out.print("Catatan Tambahan       : ");
		String notes = input.nextLine();

		System.out.print("Panjang Tubuh (cm)     : ");	
		float panjang_tubuh = input.nextFloat();
		
		System.out.print("Lebar Tubuh (cm)       : ");
		float lebar_tubuh = input.nextFloat();
		
		System.out.print("Tinggi Tubuh (cm)      : ");
		float tinggi_tubuh = input.nextFloat();
		
		System.out.print("Berat Tubuh (kg)       : ");
		float berat_tubuh = input.nextFloat();
		
		System.out.print("Jumlah Anggota Keluarga: ");
		int jumlah_orang = input.nextInt();
		
		System.out.print("Tanggal Lahir          : ");
		String tanggal_lahir = input.next();
		
		System.out.print("Jumlah Cetakan Data    : ");
		int jumlah_cetak = input.nextInt();

		float rasio = (berat_tubuh)/( (panjang_tubuh/100) * (lebar_tubuh/100) * (tinggi_tubuh/100) );

// -------------------------------------------------------------------------------------------------------------- //
		String verif_status = "";	
		String apartemen = "";
		String kabupaten = "";

		char huruf_awal = nama.charAt(1);
		int anggaran_makan = 50000 * 365 * jumlah_orang;

		String tahun = tanggal_lahir.substring(6,10);
		int tahun_lahir = Integer.parseInt(tahun);
		int umur_kepala_keluarga = 2018 - tahun_lahir;

        int jumlah_ascii= 0;

        for (int i = 0; i < nama.length(); i++){
            char huruf_nama = nama.charAt(i);
            int nilai_ascii = huruf_nama; jumlah_ascii += nilai_ascii;}
		
		float nomor_belakang_keluarga = ((panjang_tubuh*tinggi_tubuh*lebar_tubuh) + jumlah_ascii) % 10000;
		int nomor_int = Math.round(nomor_belakang_keluarga);
		String nomor_str = String.valueOf(nomor_int);
		String nomor_keluarga = (nama.charAt(0) + nomor_str);

		if (0< panjang_tubuh && panjang_tubuh <= 250 && 
			0< lebar_tubuh && lebar_tubuh <= 250 &&
			0< tinggi_tubuh && tinggi_tubuh <= 250 &&
			0< berat_tubuh && berat_tubuh <= 150 &&
			0< jumlah_orang && jumlah_orang <= 20)
		{verif_status = "pindah";}
		else
		{verif_status = "tinggal";}
		
		if (verif_status == "pindah")
			{if (19 <= umur_kepala_keluarga  && umur_kepala_keluarga <= 1018)
				{if (0 <= anggaran_makan && anggaran_makan <= 100000000)
					{apartemen = "Teksas";
					kabupaten = "Sastra";}
				else
					{apartemen = "Mares";
					kabupaten = "Margonda";}}
		
			else if (0 <= umur_kepala_keluarga && umur_kepala_keluarga <= 18)
				{apartemen = "PPMT";
				kabupaten = "Margonda";}}
// -------------------------------------------------------------------------------------------------------------- //
		System.out.println(input.nextLine());

		if (verif_status == "tinggal")
			{System.out.println("\n" +
								"CATATAN: Keluarga ini tidak perlu direlokasi!\n");};
		
		if (verif_status == "pindah")
			{System.out.println("\n" +
								"REKOMENDASI APARTEMEN\n" +
								"------------------------------------------------\n" +
								"MENGETAHUI : Identitas Keluarga: " + nama + " - " + nomor_keluarga + "\n" +
								"MENIMBANG  : Anggaran makanan tahunan: " + anggaran_makan + "\n" +
								"             Umur kepala keluarga: " + umur_kepala_keluarga + " tahun \n" +
								"MEMUTUSKAN : Keluarga " + nama + " akan ditempatkan di: \n" +
								"Apartemen " + apartemen + " , Kabupaten " + kabupaten + "\n" +
								"\n");}

		for (int i=1; i<jumlah_cetak+1; i +=1 )
		{
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan ke " + i + " dari " + jumlah_cetak + " untuk: ");
			String penerima = input.nextLine(); // Lakukan baca input lalu langsung jadikan uppercase
			
			String catatan = "";
			if (notes.length() > 1)
			{catatan = notes;}
			else
			{catatan = "Tidak ada catatan tambahan";}

			System.out.println("------------------------------------------------");
			System.out.println("DATA SIAP DICETAK UNTUK "+ penerima.toUpperCase());
			System.out.println("------------------------------------------------");
			
			System.out.println(nama + " - " + alamat);
			System.out.println("Lahir pada tanggal "+ tanggal_lahir);
			System.out.println("Rasio Berat Per Volume = " + rasio + " kg/m^3");
			System.out.println(catatan + "\n");}
	}}