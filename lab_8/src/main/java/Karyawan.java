abstract class Karyawan{                // Iniasiasi kelas abstrak untuk Karyawan dan akan digunakan sebagai superclass
    protected String nama;              // Beberapa instance variable
    protected String jabatan;
    protected int gaji;
    protected int jumlah_gajian = 0;

    // Getter
    public String getNama(){
        return this.nama;
    }

    public String getJabatan(){
        return this.jabatan;
    }

    public int getGaji(){
        return this.gaji;
    }

    // Method yang digunakan untuk gajian
    public void gajian(){
        jumlah_gajian += 1;
        if (jumlah_gajian % 6 == 0) tambahGaji();
    }

    // Method yang digunakan apabila terjadi kenaikan gaji sebesar 10% dari gaji awal
    public void tambahGaji(){
        int gaji_baru = gaji + (gaji/10);
        System.out.println(this.nama + " mengalami kenaikan gaji sebesar 10% dari " + this.gaji + " menjadi " + gaji_baru);
        this.gaji = gaji_baru;
    }
}