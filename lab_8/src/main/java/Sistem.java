import java.util.*;

public class Sistem{															// Class "SISTEM" ini layaknya class dari kantor, yang mengatur alur dari informasi data
    private ArrayList<Karyawan> karyawan = new ArrayList<Karyawan>();			// Array List yang akan diisi dengan objek dari karyawan yang telah didaftarakan

    public Karyawan cari(String nama){											// Method untuk mencari karyawan, jika ditemukan akan mereturn objeknya, jika tidak, akan mereturn null
        for (Karyawan i : karyawan){
            if (i.getNama().equals(nama)) return i;
        }
        return null;
	}

	public void tambah(String nama, String jabatan, int gaji){					// Method untuk mendaftarkan para pekerja
		if(cari(nama) != null) System.out.println("Karyawan dengan nama " + nama + " telah terdaftar");
		if(jabatan.equals("MANAGER") | jabatan.equals("Manager")) karyawan.add(new Manager(nama,gaji));	
		else if(jabatan.equals("STAFF") | jabatan.equals("Staff")) karyawan.add(new Staff(nama,gaji));
		else karyawan.add(new Intern(nama,gaji));
		System.out.println(nama + " telah bekerja sebagai " + jabatan + " di PT. TAMPAN");			// Exception handling apabila nama orang tersebut telah terdaftar sebelumnya
    }
	
	public void status(String nama){											// Method untuk mencetak status dari pekerja
	 	if(cari(nama) == null){
	 		System.out.println("Karyawan tidak ditemukan");
	 		return;
	 	}
	 	System.out.println(cari(nama).getNama() + " " + cari(nama).getGaji());
    }
    
	public void gajian(){														// Method untuk memproses apabila sudah saatnya untuk gajian
        System.out.println("Semua karyawan telah diberikan gaji");
        for (Karyawan i : karyawan){
            i.gajian();
            if (i instanceof Staff & i.getGaji() > 18000){
                i = new Manager(i.getNama(),i.getGaji());
                System.out.println("Selamat, " + i.getNama() + " telah dipromosikan menjadi MANAGER");		// Apabila total gaji melebihi batas staff, akan diberikan promosi naik jabatan
            }
        }
	}
	
	public void anak_buah(String merekrut, String direkrut){					// Method untuk menambahkan anak buah
		Karyawan atasan = cari(merekrut);
		Karyawan bawahan = cari(direkrut);
		if(atasan == null | bawahan == null){
			System.out.println("Nama tidak berhasil ditemukkan");				// Handling apabila nama yang hendak diproses tidak ditemukan
			return;
		}
		if(atasan instanceof Manager){
			System.out.println(((Manager)atasan).tambahBawahan(bawahan));		// Memproses apabila atasannya adalah seorang Manager
		}
		else if(atasan instanceof Staff){
			System.out.println(((Staff)atasan).tambahBawahan(bawahan));			// Memproses apabila atasannya adalah seorang Staff
		}
		else{
			System.out.println("Anda tidak layak memiliki bawahan");			// Handling apabila jabatan yang dimiliki tidak bisa memiliki bawahan
		}
	}
	
	
}