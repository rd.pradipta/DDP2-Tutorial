public class Manager extends Karyawan{                       // Subclass Manager yang mengextend Superclass dari class Karyawan
    private Karyawan[] bawahan = new Karyawan[10];
    private int counter = 0;
    
    public Manager(String nama, int gaji){                  // Mengoverride method di Superclass
        this.nama = nama;
        this.jabatan = "MANAGER";
        this.gaji = gaji;
    }
    public String tambahBawahan(Karyawan nama_bawahan){     // Method yang digunakan untuk menambahkan bawahan khusus bawahannya Manager
        for (int i = 0; i<10; i++){                         // Ini digunakan apabila manager sudah memiliki bawahan dengan nama tertentu
            if (bawahan[i] == null) continue;
            else if (nama_bawahan.getNama().equals(bawahan[i].getNama())){
                return "Karyawan " + nama_bawahan.getNama() + " telah menjadi bawahan " + this.nama;
            }
        }
        if(!(nama_bawahan instanceof Manager)){             // Ini digunakan apabila manager belum memiliki bawahan dengan nama orang yang diinginkan
			bawahan[counter] = nama_bawahan;
			counter += 1;
			return "Karyawan " + nama_bawahan.getNama() + " berhasil ditambahkan menjadi bawahan " + this.nama;
        }
        return "Anda tidak layak memiliki bawahan";         // Exception apabila tidak punya hak untuk memiliki bawahan
    }

}