public class Intern extends Karyawan{               // Subclass Intern yang mengextend Superclass dari class Karyawan
    public Intern(String nama, int gaji){
        this.nama = nama;
        this.jabatan = "INTERN";
        this.gaji = gaji;
    }
}