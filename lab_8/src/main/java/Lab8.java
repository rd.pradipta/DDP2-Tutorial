// Import module yang diperlukan
import java.util.*;
import java.io.*;

public class Lab8{
    public static void main(String[] args) {                                        // Bagian Main
        Sistem data = new Sistem();                                                 // Inisiasi objek
        File text = new File("input.txt");                                          // Membaca file

        try{                                                                        // Exception untuk filenya ditemukan atau tidak
            Scanner inp = new Scanner(text);                                        // Inisiasi input
            while (inp.hasNextLine()){                                              // Looping
                String[] masukan = inp.nextLine().split(" ");                       // Memisahkan masukan menjadi list yang di pecah
                
                if (masukan[0].equals("TAMBAH_KARYAWAN")) data.tambah(masukan[1], masukan[2], Integer.parseInt(masukan[3])); // Pemrosesan berdasarkan komando dari masukan
                else if (masukan[0].equals("STATUS")) data.status(masukan[1]);
                else if (masukan[0].equals("TAMBAH_BAWAHAN")) data.anak_buah(masukan[1],masukan[2]);
                else if (masukan[0].equals("GAJIAN")) data.gajian();
                else {
                    System.out.println("Maaf, Perintah Salah! Silakan Coba Lagi!");
                }
            }
        }
        catch(FileNotFoundException e){                                             // Exception handling apabila file tidak ditemukan
            System.out.println("Maaf, File Tidak Ditemukan!");
        }

    }
}

