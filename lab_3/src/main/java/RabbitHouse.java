import java.util.Scanner;

public class RabbitHouse{
	static int ans, panjang;
	public static void hitungNormal(int jml, int pjg){
		int ret = 0;
		if(pjg == 1){
			ret = jml;
			ans += ret;
			return ;
		}else{
			ret = jml;
			ans += ret;
			hitungNormal(ret * pjg,pjg-1);
			return ;
		}
	}
	public static boolean cekPalindrom(String kata){
		for(int i=0;i<kata.length()/2;i++){
			if(kata.charAt(i) != kata.charAt(kata.length()-1-i)) return false;
		}
		return true;
	}
	public static void hitungPalindrom(String kata){
		String tmp = "";
		if(kata.length() == 0) return;
		if(cekPalindrom(kata) == true){
			return;
		}else{
			ans += 1;
			int maxIdx = kata.length();
			for(int i=0;i<kata.length();i++){
				if(i == 0){
					tmp = kata.substring(1,maxIdx);
					hitungPalindrom(tmp);
				}else
				if(i == maxIdx-1){
					tmp = kata.substring(0,maxIdx-1);
					hitungPalindrom(tmp);
				}else{
					tmp = kata.substring(0,i) + kata.substring(i+1,maxIdx);
					hitungPalindrom(tmp);
				}
			}
		}
	}
	public static void main  (String[] args){
		ans = 0;
		Scanner input = new Scanner(System.in);
		String a = input.next();
		String b = input.next();
		panjang = b.length();
		if(a.charAt(0) == 'n'){
			hitungNormal(1,panjang);
			System.out.println(ans);
		}else{
			hitungPalindrom(b);
			System.out.println(ans);
		}
	}
}