package character;
import java.util.ArrayList;

public class Player{
	protected String name;
	protected int hp;
	protected ArrayList<Player> diet = new ArrayList<Player>();
	protected boolean isCooked = false;
	public Player(String name, int hp) {
		this.name = name;
		this.hp = hp;
	}
	public String getName(){
		return this.name;
	}
	public int getHp(){
		return hp;
	}
	public boolean isDead(){
		return hp<=0;
	}
	public boolean isCooked(){
		return hp<=0;
	}
	public void damaged(){
		this.hp -= 10;
		if(this.hp < 0){
			this.hp = 0;
		}
	}
	public void burned(){
		this.hp -= 10;
		if(this.isDead()){
			this.isCooked = true;
		}
		if(this.hp < 0){
			this.hp = 0;
		}
	}
	public String attack(Player korban){
		korban.damaged();
		String output = "Nyawa " + korban.getName() + " " + korban.getHp();
		if(korban.isDead()){
			output += "\n dan meninggal dunia";
		}
		return output;
	}

	public String eat(Player korban){
		if(this.canEat(korban)){
			diet.add(korban);
			this.hp += 15;
			return this.name + " memakan " + korban.getName() + "\nNyawa " + this.name + " kini " + this.hp;
		}
		return this.name + " tidak bisa memakan " + korban.getName();
	}

	public String diet(){
		String output = "";
		for (int i=0; i < diet.size() ;i++ ) {
			String temp = (diet.get(i)).getClass().getSimpleName();
			output+=temp + " " + diet.get(i).getName();
		}
		return output;
	}

	public String status(){
		String output = (this.getClass().getSimpleName()) + " " + this.name + "\nHP: " + this.hp;
		if(this.isDead()){
			output+= "\nSudah meninggal dunia dengan damai";
		} else {
			output+= "\nMasih hidup";
		}
		if(this.diet().equals("")){
			output+= "\nBelum memakan siapa siapa";
		}else {
			output+= "\nMemakan "+ this.diet();
		}
		return output;
	}

	public boolean canEat(Player korban){
		return korban.isDead();
	}
	
	public String toString(){
		return this.name;	
	}
}
