# Repository Kumpulan Submisi Lab (Tutorial) DDP 2

[![License Status](https://badges.frapsoft.com/os/mit/mit.svg?v=102)]
(https://gitlab.com/rd.pradipta/open-source-badge/)
[![Build Status](https://travis-ci.org/freeCodeCamp/how-to-contribute-to-open-source.svg?branch=master)](https://travis-ci.org/freeCodeCamp/how-to-contribute-to-open-source)

Dasar-dasar Pemrograman 2 - CSGE601021 | Fakultas Ilmu Komputer, Universitas Indonesia, Semester Genap 2017/2018

* Nama    : Rd Pradipta Gitaya S
* NPM     : 1706043361
* Kelas   : DDP 2 Kelas E
* Email   : rd.pradipta@ui.ac.id
* Jangan lupa, kunjungi juga GitHub saya di: [Link GitHub](https://github.com/diptags)

Tambahan: Saya sedang iseng buat LINE bot, dan saya butuh panduan dari para master, mungkin para master bisa memberikan saya saran :) Hehehe, link source codenya ada di link GitHub

## Daftar Isi

Repository ini akan berisi submisi dari soal Lab (Tutorial) DDP 02

1. [Submisi Lab 1](https://gitlab.com/rd.pradipta/DDP2-Tutorial/tree/master/lab_1) - Pengenalan Java & Git

2. [Submisi Lab 2](https://gitlab.com/rd.pradipta/DDP2-Tutorial/tree/master/lab_2) - Konsep Dasar Pemrograman Java

3. [Submisi Lab 3](https://gitlab.com/rd.pradipta/DDP2-Tutorial/tree/master/lab_3) - Rekursif

4. [Submisi Lab 4](https://gitlab.com/rd.pradipta/DDP2-Tutorial/tree/master/lab_4) - Object Oriented Programming

5. [Submisi Lab 5](https://gitlab.com/rd.pradipta/DDP2-Tutorial/tree/master/lab_5) - Array

6. [Submisi Lab 6](https://gitlab.com/rd.pradipta/DDP2-Tutorial/tree/master/lab_6) - Studi Kasus OOP

7. [Submisi Lab 7](https://gitlab.com/rd.pradipta/DDP2-Tutorial/tree/master/lab_7) - Inheritance

8. [Submisi Lab 8](https://gitlab.com/rd.pradipta/DDP2-Tutorial/tree/master/lab_8) - Polymorphism

9. [Submisi Lab 9](https://gitlab.com/rd.pradipta/DDP2-Tutorial/tree/master/lab_9) - Packaging & API

10. [Submisi Lab 10](https://gitlab.com/rd.pradipta/DDP2-Tutorial/tree/master/lab_10) - Exceptions
