public class BingoCard {

// Tidak mengubah isi dari template untuk bagian ini 
	private Number[][] numbers = new Number[5][5];
	private Number[] numberStates = new Number[101];
	private boolean isBingo = false;

	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}
// ------------------------------------------------------------- //

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

// ------------------------------------------------------------- //

	public String markNum(int num){ // Ketika memasukkan keyword "MARK"
		if(this.numberStates[num] ==  null)
		{
			return "Kartu tidak memiliki angka " + num;
		}
		else if(this.numberStates[num].isChecked())
		{
			return num + " sebelumnya sudah tersilang";
		}
		else
		{
			int x = this.numberStates[num].getX();
			int y = this.numberStates[num].getY();
	
			this.numbers[x][y].setChecked(true);
			this.numberStates[num].setChecked(true);
			this.isBingo = this.bingoCheck(); // Masuk ke fungsi untuk cek Bingo
			return num + " tersilang";
		}

	}
	public String info(){ // Ketika memasukkan keyword "INFO"
		String str = ""; // Loop Mencetak
		for(int i=0;i<5;i++){
			str +='|';
			for(int j=0;j<5;j++){
				str += this.numbers[i][j].isChecked()?" X ": " "+this.numbers[i][j].getValue();
				str +=" |";
			}
			if(i!=4)str += "\n";
		}
		return str;
	}

	public void restart(){ // Untuk fungsi ketika memasukkan keyword "RESTART"
		for(int i=0;i<5;i++){ // Loop Mereset status boolean dari apakah sudah di X atau belum
			for(int j=0;j<5;j++){
				this.numbers[i][j].setChecked(false); // Loop mengubah status Array 2 dimensi menjadi False (tidak di X)
			}
		}
		for(int i=0;i<100;i++){
			if(this.numberStates[i] == null)continue; // Loop mengubah NumberStates menjadi false
			this.numberStates[i].setChecked(false);
		}
		System.out.println("Mulligan!");
	}

	public boolean bingoCheck(){
		boolean status; // Inisiasi boolean

		for(int i=0;i<5;i++){ // Pengecekan diagonal kiri
			status = true;
			for(int j=0;j<5;j++){
				if(!this.numbers[i][j].isChecked()){
					status = false;
					break;
				}
			}
			if(status)return status;
		}

		for(int j=0;j<5;j++){ // Pengecekan horizontal
			status = true;
				for(int i=0;i<5;i++){
				if(!this.numbers[i][j].isChecked()){
					status = false;
					break;
				}
			}
			if(status)
			{
				return status;
			}
		}

		status = true; // Pengecekan vertikal
		for(int i=0;i<5;i++){
			if(!this.numbers[i][i].isChecked()){
				status = false;break;
			}
		}
		if(status)return status;

		status = true;	// Pengecekan diagonal kanan
		for(int i=0;i<5;i++){
			if(!this.numbers[i][4-i].isChecked()){
				status = false;
				break;
			}
		}
		return status;
	}
}
