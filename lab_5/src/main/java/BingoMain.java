import java.util.*;

public class BingoMain{
    
    static BingoCard card;

	public static void main(String[] args){
		
        int[][] kartu = new int[5][5]; // Array kosong 5x5
        String[] masukan_split; // Array untuk masukan yang sudah di split
        Scanner inp = new Scanner(System.in); // Input
        String masukan; // Inisiasi masukan
        boolean bingostatus = false;
        
        while(!bingostatus){
            
        masukan_split = inp.nextLine().split(" ");

        if(masukan_split.length > 2){
            for (int i = 0; i < 5; i++){
                kartu[0][i] = Integer.parseInt(masukan_split[i]);
               }
   
           for (int baris = 1; baris < kartu.length; baris++) {
               masukan_split = inp.nextLine().split(" ");
               
               for (int kolom = 0; kolom < masukan_split.length; kolom++) {
                   kartu[baris][kolom] = Integer.parseInt(masukan_split[kolom]);
               }
           }
        }
        else{
			if(masukan_split[0].toUpperCase().equals("MARK")){
                System.out.println(card.markNum(Integer.parseInt(masukan_split[1])));
			}
			else if(masukan_split[0].toUpperCase().equals("INFO")){
				System.out.println(card.info());
			}
			if(masukan_split[0].toUpperCase().equals("RESTART")){
				card.restart();
			}
        }

        // Sebenarnya tidak paham bagian sini, tapi ini dari copas dari file test //

        Number[][] numbers = {{new Number(kartu[0][0],0,0), new Number(kartu[0][1],0,1),new Number(kartu[0][2],0,2),new Number(kartu[0][3],0,3),new Number(kartu[0][4],0,4)},
        {new Number(kartu[1][0],1,0), new Number(kartu[1][1],1,1),new Number(kartu[1][2],1,2),new Number(kartu[1][3],1,3),new Number(kartu[1][4],1,4)},
        {new Number(kartu[2][0],2,0), new Number(kartu[2][1],2,1),new Number(kartu[2][2],2,2),new Number(kartu[2][3],2,3),new Number(kartu[2][4],2,4)},
        {new Number(kartu[3][0],3,0), new Number(kartu[3][1],3,1),new Number(kartu[3][2],3,2),new Number(kartu[3][3],3,3),new Number(kartu[3][4],3,4)},
        {new Number(kartu[4][0],4,0), new Number(kartu[4][1],4,1),new Number(kartu[4][2],4,2),new Number(kartu[4][3],4,3),new Number(kartu[4][4],4,4)}};

		Number[] states = new Number[100];
        for(int i=0; i<5; i++){
			for(int j=0; j<5; j++){				
				states[numbers[i][j].getValue()] = numbers[i][j];
			}
        }
        card = new BingoCard(numbers,states);
    }
	}
}