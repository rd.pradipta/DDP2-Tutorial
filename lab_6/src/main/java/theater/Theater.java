/**
 * @author Rd Pradipta Gitaya S
 * @version 1.0
*/
package theater;

import movie.Movie;
import ticket.Ticket;

import java.util.ArrayList;
import java.util.Arrays;

//import ticket.Ticket;

public class Theater {

    private String place; // Nama Bioskop
    private int cash; // Kas yang ada
    
    private ArrayList<Ticket> ticketlist; // MOvie dan keterangan tayang
    public Movie[] moviedesc; // Komponen deskripsi dari suatu film

    public Theater(String place, int cash, ArrayList<Ticket> ticketlist, Movie[] moviedesc){
        this.place = place; // Nama - nama bioskop
        this.cash = cash; // Total Kas awal
        this.ticketlist = ticketlist; //Array movie list
        this.moviedesc = moviedesc; // Array isinya tiker
    }
  
    public String getPlace(){
 		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

    public int getCash() {
		return cash;
	}

	public void setCash(int cash) {
		this.cash = cash;
    }
    
    public ArrayList<Ticket> getTicketlist(){
        return ticketlist;
    }
    
    public void setTicketlist(ArrayList<Ticket> ticketlist){
        this.ticketlist = ticketlist;
    }

    public void printInfo(){

        // Mencari judul dan memasukkan dalam suatu array baru
        String[] movielist = new String[this.moviedesc.length]; // Buat array baru

        for (int i=0; i<this.moviedesc.length; i++){
            movielist[i] = this.moviedesc[i].getTitle();
        }

        String movielist_final = Arrays.toString(movielist);
        movielist_final = movielist_final.replace("[","");
        movielist_final = movielist_final.replace("]","");

        System.out.println("------------------------------------------------------------------");
        System.out.println("Bioskop\t\t\t: " + this.place);
        System.out.println("Saldo Kas\t\t: " + this.cash);
        System.out.println("Jumlah tiket tersedia\t: " + String.valueOf(this.ticketlist.size()));
        System.out.println("Daftar Film tersedia\t: " + movielist_final);
        System.out.println("------------------------------------------------------------------");
    }

    public static void printTotalRevenueEarned(Theater[] theatersummary){
        int totalcash = 0;
        for (int i=0; i< theatersummary.length; i++){
        totalcash += theatersummary[i].getCash();
        }
        
        System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + totalcash);
        System.out.println("------------------------------------------------------------------");
        for (int i=0; i< theatersummary.length; i++){
            
            System.out.println("Bioskop\t\t: " + theatersummary[i].getPlace());
            System.out.println("Saldo Kas\t: " + theatersummary[i].getCash());
            System.out.println("");
        }
        System.out.println("------------------------------------------------------------------");
    }

    public boolean checkTiket(Movie film) {
        boolean ticket_check = false;
        for(Movie isi : moviedesc) {
            if (isi.equals(film)) {
                ticket_check = true;
                break;
            }
        }
        return ticket_check;
    }
}
