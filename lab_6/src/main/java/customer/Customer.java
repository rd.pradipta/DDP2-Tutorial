/**
 * @author Rd Pradipta Gitaya S
 * @version 1.0
*/

package customer;

import movie.Movie;
import theater.Theater;
import ticket.Ticket;

import java.util.ArrayList;
import java.util.Arrays;

public class Customer {

    private String name;
    private boolean gender; // True = Cewe , False = Cowo
    private int age;

    public Customer(String name, boolean gender, int age){
        this.name = name;
        this.gender = gender;
        this.age = age;
    }
// -------------------------------------------------------- //

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public boolean getGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
    }
    
    public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
    }
    
// -------------------------------------------------------- //

public Ticket orderTicket(Theater bioskop, String namaFilm, String hari, String jenis) {
    int count = 0;
    Ticket beli = null;
    boolean movietype = false;

    if (jenis.equals("Biasa")) movietype = false;
    else movietype = true;
    
    for (Ticket tiket : bioskop.getTicketlist()) {
        String tmpNama = tiket.getData().getTitle();
        String tmpHari = tiket.getDay();
        String tmpJenis = tiket.getType();
        
        if (tmpNama.equals(namaFilm) &&
            tmpHari.equals(hari) &&
            tmpJenis.equals(jenis)) {
            
                if(this.age >= tiket.getData().MinUmur())
                {
                System.out.println(this.name + " telah membeli tiket " + tmpNama + " jenis " + jenis + " di " + bioskop.getPlace() + " pada hari " + tiket.getDay() + " seharga Rp. " + tiket.ticketPrice());
                bioskop.setCash(bioskop.getCash() + tiket.ticketPrice());
                beli = tiket;
                break;
                }  
                
                else
                {
                System.out.println(this.name + " masih belum cukup umur untuk menonton " + tiket.getData().getTitle() + " dengan rating " + tiket.getData().getRating());
                break;
                }

        } else {
            count+=1;
        }
    }
    if (count == bioskop.getTicketlist().size()) {
        System.out.println("Tiket untuk film " + namaFilm + " jenis " + jenis + " dengan jadwal " + hari + " tidak tersedia di " + bioskop.getPlace());
    }
    return beli;
}

    
    public void findMovie(Theater theaterdata, String title){
        // Di method ini ngecek film ada ga di bioskop, kalo ada print error, kalo ada oper ke Movie

        // Buat array isinya judul film yang ada dari array movielist
        String[] movielist = new String[theaterdata.moviedesc.length];
        
        int counter_movie = 0; // Mengecek ada atau tidak filmnya
        int counter_obj = 0; // Mengecek indeks ke berapa yang ada movienya
        
        for (int i=0;i<theaterdata.moviedesc.length; i++){
            movielist[i] = theaterdata.moviedesc[i].getTitle();
        }

        for (int j=0; j<movielist.length; j++){
            if (movielist[j] == title){
                counter_movie += 1;
                counter_obj += j;
            }
            else{
                continue;
            }
        }
        
        if (counter_movie == 0){
            System.out.println("Film " + title + " yang dicari " + this.name + " tidak ada di bioskop " + theaterdata.getPlace());
        }
        else {        
        String rating = theaterdata.moviedesc[counter_obj].getRating();
        int duration = theaterdata.moviedesc[counter_obj].getDuration();
        String genre = theaterdata.moviedesc[counter_obj].getGenre();
        String moviestatus = theaterdata.moviedesc[counter_obj].getMoviestatus();

        Movie movie_info = new Movie(title,rating,duration,genre,moviestatus); // Membuat objek baru untuk keperluan print
        movie_info.PrintDesc();

        }
    }
}