/**
 * @author Rd Pradipta Gitaya S
 * @version 1.0
*/

package movie;

import java.util.ArrayList;
import java.util.Arrays;

public class Movie {

    private String title;
    private String rating;
    private int duration;
    private String genre;
	private String moviestatus;
	private int minimalUmur;

    public Movie(String title, String rating, int duration, String genre, String moviestatus){
        this.title = title;
        this.rating = rating;
        this.duration = duration;
        this.genre = genre;
        this.moviestatus = moviestatus;
    }

    public int MinUmur() {
        switch (this.rating) {
            case "Dewasa":
                return 17;
            case "Remaja":
                return 13;
            default:
                return 0;
        }
    }

    public String getTitle() {
		return title;
	}

    public int getMinimalUmur() {
        return minimalUmur;
    }

    public String getRating() {
		return rating;
	}

    public int getDuration() {
		return duration;
	}
    
    public String getGenre() {
		return genre;
	}

    public String getMoviestatus() {
		return moviestatus;
	}

	public void PrintDesc(){
		System.out.println("------------------------------------------------------------------");
        System.out.println("Judul	: " + this.getTitle());
        System.out.println("Genre	: " + this.getGenre());
        System.out.println("Durasi  : " + String.valueOf(this.getDuration()) + " menit");
        System.out.println("Rating  : " + this.getRating());
        System.out.println("Jenis   : " + "Film " + this.getMoviestatus());
        System.out.println("------------------------------------------------------------------");
	}
}