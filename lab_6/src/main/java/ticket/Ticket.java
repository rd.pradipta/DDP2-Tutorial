/**
 * @author Rd Pradipta Gitaya S
 * @version 1.0
*/
package ticket;

import movie.Movie;

public class Ticket {

    private Movie data; // Objek dari Movies
    private String day; // Hari penayangan
    private boolean type; // 3d = True, Biasa = FALSE
    private final int TICKETPRICE = 60000; // Harga tiket standar

    public Ticket(Movie data, String day, boolean type){
        this.data = data; // Isinya objek dari array movies
        this.day = day;
        this.type = type;
    }

    public Movie getData() {
		return data;
	}

    public String getDay() {
		return day;
	}
    
    public String getType(){
        if (type) return "3 Dimensi";
        else
        {
            return "Biasa";
        }
    }

    public int ticketPrice() {
        int price = TICKETPRICE;

        if (day.equals("Sabtu") || day.equals("Minggu")) {
            price += 40000;
        }
        if (type) {
            price = price + (int)(price*0.2);
        }
        return price;
    }
}