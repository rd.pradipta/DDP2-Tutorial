package character;

public class Magician extends Player{
    
    public Magician(String chara, int hp){
        super(chara,"Magician",hp);
    }

    public String burn(Player attacked){
        if (attacked.getTipe().equals("Magician")) attacked.setHP(attacked.getHp() - 20);
        else attacked.setHP(attacked.getHp() - 10);

        if (attacked.getHp() == 0){
            attacked.setMatang(true);
            return "=======================================\n" + "Nyawa " + attacked.getName() + " 0\n" + "dan matang";
        }
        return "Nyawa " + attacked.getName() + " " + attacked.getHp();
    }

}