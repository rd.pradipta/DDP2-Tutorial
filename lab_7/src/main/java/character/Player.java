package character;

import java.util.*;
public class Player{

    private String name;
    private String tipe;
    private int hp;
    private String roar;
    private ArrayList<Player> diet = new ArrayList<Player>();
    private boolean isSudahMatang = false;

    public Player(String name, String tipe, int hp){
        this.name = name;
        this.tipe = tipe;
        this.hp = hp;
        this.diet = diet;
        this.isSudahMatang = isSudahMatang;
    }

    public Player(String name, String tipe, int hp, String roar){
        this.name = name;
        this.tipe = tipe;
        this.hp = hp;
        this.roar = roar;
        this.diet = diet;
        this.isSudahMatang = isSudahMatang;
    }

    public boolean canEat(Player eaten){
        if (this.getTipe().equals("Human") | this.getTipe().equals("Magician")){
            if (eaten.getMatang() == true ){
                if (eaten.getTipe().equals("Human") | eaten.getTipe().equals("Magician")) return false;
                return true; // Prevent Cannibalism
            }
            else{
                return false; // If the food doesnt burnt enought
            }
        }
        
        else if (eaten.getHp() != 0) return false;
        return true;
    }

    public String getStatus(){
        String status_a;
        String status_b;

        if (this.hp == 0) status_a = "Sudah meninggal dunia dengan damai";
        else status_a = "Masih hidup";
        
        if (this.diet.size() == 0) status_b = "Belum memakan siapa siapa";
        else status_b = "Memakan " + this.getDiet();
        
        return this.tipe + " " + this.name + "\n" +
               "HP: " + this.hp + "\n" +
               status_a + "\n" +
               status_b;
    }

    public String attack(Player attacked){
        if (attacked.getTipe().equals("Magician")) attacked.setHP(attacked.getHp() - 20);
        else attacked.setHP(attacked.getHp() - 10);
   
        return "Nyawa " + attacked.getName() + " " + attacked.getHp();
    }
    public String burn(Player attacked) {
		return this.burn(attacked);
	}

    public boolean getMatang(){
        return this.isSudahMatang;
    }

    public void setMatang(boolean status){
        this.isSudahMatang = status;
    }

    public String getName(){
        return this.name;
    }
    public String getTipe(){
        return this.tipe;
    }
    public int getHp(){
        if (this.hp <= 0){
            this.hp = 0;
        }
        return this.hp;
    }
    public void setHP(int hp){
        this.hp = hp;
    }

    public String getRoar(){
        return this.roar;
    }

    public String getDiet(){
        String eatenlist = "";
        for (Player i : this.diet){
            eatenlist = eatenlist + i.getTipe() + " " + i.getName();
        }
        return eatenlist;
    }
   
    public void setDiet(Player eaten){
        this.diet.add(eaten);
    }

    

}
