package character;

public class Monster extends Player{

    public Monster(String chara, int hp){
        super(chara,"Monster",hp*2,"AAAAAAaaaAAAAAaaaAAAAAA");
    }
    public Monster(String chara, int hp, String roar){
        super(chara,"Monster",hp*2,roar);
    }

    public String roar(){
        return "AAAAAAaaaAAAAAaaaAAAAAA";
    }

}
//  write Monster Class here