import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (Player i:player){
            if (i.getName() == name) return i;
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     */
    public String add(String chara, String tipe, int hp){
        if (find(chara) == null){
            if(tipe == "Human"){
                Human human = new Human(chara,hp);
                player.add(human);
                return "=======================================\n" +
                        chara + " ditambah ke game";
                }
            else if (tipe == "Magician"){
                Magician magician = new Magician(chara,hp);
                player.add(magician);
                return "=======================================\n" + 
                        chara + " ditambah ke game";
            }
            else if (tipe == "Monster"){
                return add(chara,tipe,hp,"AAAAAAaaaAAAAAaaaAAAAAA");
            }    
        }
            return "=======================================\n" + 
                   "Sudah ada karakter bernama " + chara;
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){        
        if (find(chara) == null){
            Monster pemain = new Monster(chara, hp, roar);
            player.add(pemain);
            return "=======================================\n" + 
                    chara + " ditambah ke game";
        }
        else{
            return "=======================================\n" + 
                   "Sudah ada karakter bernama " + chara;
        }
    }

    /**\
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){ // ADA SEDIKIT BUG DI SINI. NANTI DIPERBAIKI
        Player status = find(chara);
        boolean keberadaan = true;

        if (status != null){
            for (int i=0;i<player.size();i++){
                if (player.get(i).getName().equals(status.getName())){
                    player.remove(i);
                    keberadaan = false;
                    break;
                }
            }
        }
        if (keberadaan == false & status != null) return "=======================================\n" + chara + " dihapus dari game";

        else{
            return "=======================================\n" + 
                   "Tidak ada " + chara;
        }
           
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
       return "=======================================\n" + find(chara).getStatus();
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String completelist = "";
        for (Player i : player) completelist += find(i.getName()).getStatus() + "\n";
        return completelist;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        return "=======================================\n" + find(chara).getDiet();
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String completelist = "";
        for (Player i : player){
            if (i.getDiet() != null) completelist = completelist + (i.getName() + " : " + i.getDiet() + "\n");
            continue;
        }
        return completelist;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player mystatus = find(meName);
        Player enemy = find(enemyName);
        return "=======================================\n" + mystatus.attack(enemy);
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        Player mystatus = find(meName);
        Player enemy = find(enemyName);
        return mystatus.burn(enemy);
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player mystatus = find(meName);
        Player enemy = find(enemyName);

        mystatus.canEat(enemy);

        if (mystatus.canEat(enemy) == true){
            mystatus.setHP(mystatus.getHp() + 15);
            mystatus.setDiet(enemy);
            remove(enemyName);
            return "=======================================\n" +
                    meName + " memakan " + enemyName +"\n" + "Nyawa " + meName + " kini " + mystatus.getHp();
        }
        return "=======================================\n" +
                meName + " tidak bisa memakan " + enemyName;
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        Player status = find(meName);

        if (status == null) return "Tidak ada " + meName;
        else if (status.getTipe().equals("Human") | status.getTipe().equals("Magician")) {
            return "=======================================\n" +
                    meName + " tidak bisa berteriak";
        }
        else{
            return "=======================================\n" +
                    find(meName).getRoar();
        }
    }
}
