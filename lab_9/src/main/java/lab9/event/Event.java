/**
 * @author Rd Pradipta Gitaya S
 */

package lab9.event;

// Import required modules
import java.util.Calendar;
import java.math.BigInteger;
import java.text.SimpleDateFormat;

/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event>
{
    /** Name of event */
    private String name;
    private Calendar start_time;
    private Calendar stop_time;
    private BigInteger cost_hour;
    
    public Event(String name, Calendar start_time, Calendar stop_time, BigInteger cost_hour){
        this.name = name;
        this.start_time = start_time;
        this.stop_time = stop_time;
        this.cost_hour = cost_hour;
    }

    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    
    public String getName()
    {
        return this.name;
    }

    /**
    * Accessor for cost field. 
    * @return amount cost / hour
    */

    public BigInteger getCost(){
        return this.cost_hour;
    }

    /**
    * Print object into String 
    * @return String depends on the object
    */

    public String toString(){
        SimpleDateFormat time = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
        String return_str = this.name
                + "\n" + "Waktu mulai: " + time.format(start_time.getTime())
                + "\n" + "Waktu selesai: " + time.format(stop_time.getTime())
                + "\n" + "Biaya kehadiran: " + cost_hour;
        return return_str;     
    }

    /**
    * Method to check if overlapping or not (Conflict in schedule)
    * @return boolean true or false
    */

    public boolean overlap(Event event) {
        return !this.start_time.after(event.stop_time) && !this.stop_time.before(event.start_time);
    }

    /** Don't really understand about this, because it comes from API
    * @return 0 if nothing, 1 if true, and -1 if false
    */

    public int compareTo(Event event) {
        if (this.start_time.equals(event.start_time)) {
            return 0;
        }
        if (this.start_time.after(event.start_time)) {
            return 1;
        }
        return -1;
    }
}
