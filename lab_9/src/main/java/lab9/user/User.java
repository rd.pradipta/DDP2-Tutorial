/**
 * @author Rd Pradipta Gitaya S
 */

package lab9.user;
import lab9.event.Event;

// Import required modules

import java.util.ArrayList;
import java.math.BigInteger;
import java.util.Collections;

/**
* Class representing a user, willing to attend event(s)
*/
public class User
{
    /** Name of user */
    private String name;
    
    /** List of events this user plans to attend */
    private ArrayList<Event> events;
    
    /**
    * Constructor for User Class
    * Initializes a user object with given name and empty event list
    */
    
    public User(String name)
    {
        this.name = name;
        this.events = new ArrayList<>();
    }
    
    /**
    * Accessor for name field
    * @return name of this instance
    */

    public String getName() {
        return name;
    }
    
    /**
    * Adds a new event to this user's planned events, if not overlapping
    * with currently planned events.
    * @param event the object of event 
    * @return true if the event if successfully added, false otherwise
    */

    public boolean addEvent(Event newEvent)
    {
        //check if new Event conflicts
        for (Event i : events) {
            if (i.overlap(newEvent)) {
                return false;
            }
        }
        events.add(newEvent);
        return true;
    }

    /**
    * Returns the list of events this user plans to attend,
    * Sorted by their starting time.
    * Note: The list returned from this method is a copy of the actual
    *       events field, to avoid mutation from external sources
    * @return list of events this user plans to attend
    */
    
    public ArrayList<Event> getEvents()
    {
        ArrayList<Event> sorted = (ArrayList<Event>) events.clone();
        Collections.sort(sorted);
        return sorted;
    }

    /**
     * Method to get total cost spend by each user by doing specific schedule
     * @return big integer about total cost
     */

    public BigInteger getTotalCost() {
        BigInteger total = new BigInteger(String.valueOf(0));
        for (Event i : events) {
            total = total.add(i.getCost());
        }
        return total;
    }
}