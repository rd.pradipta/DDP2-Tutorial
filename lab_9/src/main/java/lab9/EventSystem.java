/**
 * @author Rd Pradipta Gitaya S
 */

package lab9;

import lab9.user.User;
import lab9.event.Event;

/* Import required modules */

import java.util.Calendar;
import java.util.ArrayList;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */

    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    // -------------------------------------- //

    /**
     * Method that used to add user into users array list
     * @param name of user
     * @return string if success adding into array list or not
     */

    public String addUser(String name)
    {
        if(getUser(name) == null){
            User user = new User(name);
            users.add(user);
            return "User " + name + " berhasil ditambahkan!";
        }
        return "User " + name + " sudah ada!";
    }

    /**
     * Method to get an object of specific user by iterating it in array list
     * @param name of user
     * @return object of the user if found, and NULL if not found
     */

    public User getUser(String name){
        for (User i : users){
            if (i.getName().equals(name)) return i;
        }
        return null;
    }

    // -------------------------------------- //
    
    /**
     * Method to add event into array list of object of events
     * @param name,startTimeStr,endTimeStr,costPerHourStr that will be used in the method
     * @return string to show if event is successfully added or not
     */
    
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
        if (getEvent(name) != null) return "Event " + name + " sudah ada!";

            SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            Calendar startTime = Calendar.getInstance();
            Calendar stopTime = Calendar.getInstance();
    
            try {
                startTime.setTime(time.parse(startTimeStr));
            } catch (ParseException e) {
                e.printStackTrace();
            }
    
            try {
                stopTime.setTime(time.parse(endTimeStr));
            } catch (ParseException e) {
                e.printStackTrace();
            }
    
            if (stopTime.before(startTime)) {                   // Jika waktu selesai < waktu dimulai
                return "Waktu yang diinputkan tidak valid!";
            }
            else{
            
            BigInteger costHour = new BigInteger(costPerHourStr);
            Event event = new Event(name, startTime, stopTime, costHour);

            events.add(event);
            
            return "Event " + name + " berhasil ditambahkan!";
            }
    }

    /**
     * Method to get an object of specific event by iterating it in array list
     * @param name of event
     * @return object of the event if found, and NULL if not found
     */

    public Event getEvent(String name){
        for (Event i : events){
            if (i.getName().equals(name)) return i;
        }
        return null;
    }

    // -------------------------------------- //

    /**
     * Method to add schedule into specific registered users
     * @param userName,eventName that contains username and event that will be added
     * @return string that show status if add is success or not
     */
    
    public String registerToEvent(String userName, String eventName)
    {
        // Verification when event either or person are exists

        if (getUser(userName) == null & getEvent(eventName) == null){
            return "Tidak ada pengguna  dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        }
        if (getUser(userName) == null){
            return "Tidak ada pengguna dengan nama " + userName + "!";
        }
        if (getEvent(eventName) == null){
            return "Tidak ada acara dengan nama " + eventName + "!";
        }

        Event event = getEvent(eventName);
        User user = getUser(userName);

        if (user.addEvent(event) == true){
            return userName + " berencana menghadiri " + eventName + "!";          
        }
        return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
    }
}